# https://hub.docker.com/r/oriontvv/unity3d-android-ci/

FROM gableroux/unity3d:2018.3.7f1-android

MAINTAINER Vassiliy Taranov <taranov.vv@gmail.com>


# unity 2018 needs jdk 8 https://docs.unity3d.com/Manual/android-sdksetup.html
RUN apt update && \
    apt install -y software-properties-common && \
    add-apt-repository ppa:openjdk-r/ppa && \
    add-apt-repository ppa:cwchien/gradle && \
    apt update && \
    apt install -y \
      curl \
      git \
      gradle \
      openjdk-8-jdk \
      unzip


ENV JAVA_HOME /usr/lib/jvm/java-8-openjdk-amd64/jre/
ENV PATH ${PATH}:/usr/lib/jvm/java-8-openjdk-amd64/jre/bin
ENV ANDROID_HOME /opt/android-sdk-linux

ENV PATH ${PATH}:${ANDROID_HOME}/tools:${ANDROID_HOME}/platform-tools


# newer SDK versions https://stackoverflow.com/questions/37505709/how-do-i-download-the-android-sdk-without-downloading-android-studio
RUN cd /opt && \
    wget -q https://dl.google.com/android/repository/sdk-tools-linux-4333796.zip -O android-sdk.zip && \
    unzip -q android-sdk.zip -d android-sdk-linux && \
    rm -f android-sdk.zip && \
    ls -ahl android-sdk-linux


RUN chmod -R 755 .${ANDROID_HOME}/tools/*

# ------------------------------------------------------
# --- Install Android SDKs and other build packages
# https://developer.android.com/studio/command-line/sdkmanager

# platform-tools,extra-android-support
RUN ${ANDROID_HOME}/tools/bin/sdkmanager "platform-tools"

# SDKs
# android-26
RUN ${ANDROID_HOME}/tools/bin/sdkmanager "platforms;android-26"

# accept license
RUN yes | ${ANDROID_HOME}/tools/bin/sdkmanager --licenses

RUN gradle -v

# Clean up
RUN rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN df -h
